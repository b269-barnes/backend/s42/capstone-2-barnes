const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));


// Database Connection //

mongoose.connect("mongodb+srv://mjbarnes03:admin123@cluster0.kasjqle.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
}
);

mongoose.connection.once("open", () => console.log('Now connected to the database!'));

app.use("/users", userRoute);


app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`))


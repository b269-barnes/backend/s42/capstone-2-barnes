const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		return : [true, "Product is required"]
	},
	description: {
		type: String,
		return : [true, "Description is required"]
	},
	price: {
		type: Number,
		required : [true , "Price is required"]
	},
	isActive : {
		type: Boolean,
		default : true
	}, createdOn : {
		type : Date,
		dafault : new Date()
	}
 
})

module.exports = mongoose.model("Product", productSchema);
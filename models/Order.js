const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId : {
		type: String,
		required: [true, "User ID is required"]
	},
	products : {
		productId : {
			type: String,
			required : [true, "Product ID is required"]
		},
		quantity : {
			type: Number,
			required : [true, "Quatity is required"]

		}
	},
	 totalAmount : {
	 	type: Number,
	 	required : [true, "Total amount must be a non-negative number"]

	 }

	 

})

module.exports = mongoose.model("Order", orderSchema);
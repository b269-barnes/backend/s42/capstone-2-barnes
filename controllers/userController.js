const User = require("../models/User");
const Course = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false;
		};
	});
};

// User Registration

module.exports.registerUser = (reqBody) => {
let newUser = new User({
	fullName: reqBody.fullName,
	email : reqBody.email,
	password : bcrypt.hashSync(reqBody.password,10)
});
return newUser.save().then((user,error) => {
	if (error) {
		return false;
	} else {
		return true;
	};
});


};

